" Restore cursor position to where it was before
augroup JumpCursorOnEdit
    au!
    autocmd BufReadPost *
                \ if expand("<afile>:p:h") !=? $TEMP |
                \     if line("'\"") > 1 && line("'\"") <= line("$") |
                \         let JumpCursorOnEdit_foo = line("'\"") |
                \         let b:doopenfold = 1 |
                \         if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
                \             let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
                \             let b:doopenfold = 2 |
                \         endif |
                \         exe JumpCursorOnEdit_foo |
                \     endif |
                \ endif
    " Need to postpone using "zv" until after reading the modelines.
    autocmd BufWinEnter *
                \ if exists("b:doopenfold") |
                \     exe "normal zv" |
                \     if(b:doopenfold > 1) |
                \         exe  "+".1 |
                \     endif |
                \     unlet b:doopenfold |
                \ endif
augroup END

set nocompatible

" Shows what you are typing as a command
set showcmd

" Sets lines of history VIM remembers
set history=666

" File-type highlighting and configuration.
" Run :filetype (without args) to see what you may have
" to turn on yourself, or just set them all to be sure.
syntax on
filetype on
filetype plugin on
filetype indent on

let mapleader = ','
let g:mapleader = ','

" Fast saving
nmap <leader>w :w<cr>

" TODO Temp setting
nmap <silent> <leader>= :source c:\users\siyang\_vimrc<cr>

" Wildmenu settings
set wildmenu
set wildmode=list:longest,full

" Hides buffers when they are abandoned
set hidden

" Sets visible lines around cursor
set scrolloff=3

" Always shows current position
set ruler

" Intuitive backspacing in insert mode
set backspace=indent,eol,start

" Highlight search terms...
set hlsearch
set incsearch " ...dynamically as they are typed.
nmap <silent> <leader>n :silent :nohlsearch<CR>

" Regex magic to reduce escaping of chars
set magic
" Search case settings
set ignorecase
set smartcase
" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
map N Nzz
map n nzz

" Toggle whitespace visibility
set listchars=tab:>-,trail:�,eol:$
nmap <silent> <leader>s :set nolist!<CR>

set shortmess=atI
set cmdheight=2

" Tabbing behavior
set smartindent
set shiftwidth=4
set softtabstop=4
set expandtab

" Use en for spellchecking, off by default
if version >= 700
    set spl=en spell
    set nospell
endif

" Enable mouse support in console
set mouse=a

" Shortcut for listing buffers
nnoremap <F5> :buffers<CR>:buffer<Space>

colorscheme evening

" matchit
runtime macros/matchit.vim

" Line number settings
set number
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
" Sets line number modes depending on context
set rnu
au BufEnter * :set rnu
au BufLeave * :set nu
au WinEnter * :set rnu
au WinLeave * :set nu
au InsertEnter * :set nu
au InsertLeave * :set rnu
au FocusLost * :set nu
au FocusGained * :set rnu

" More logical up/downs with g...
nnoremap <silent> k gk
nnoremap <silent> j gj
"inoremap <silent> <Up> <Esc>gka
"inoremap <silent> <Down> <Esc>gja

" Create Blank Newlines and stay in Normal mode
nnoremap <silent> zj o<Esc>
nnoremap <silent> zk O<Esc>

set foldmethod=syntax
set foldlevelstart=99
