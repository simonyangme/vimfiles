if exists("cs_curly_error")
    syn match csCurlyError "}"
    syn region csBlock start="{" end="}" contains=ALLBUT,csCurlyError,csString fold
else
    syn region csBlock start="{" end="}" transparent fold
endif
